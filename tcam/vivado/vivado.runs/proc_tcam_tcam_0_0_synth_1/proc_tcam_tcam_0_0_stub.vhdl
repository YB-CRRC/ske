-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Tue Feb 16 14:39:41 2021
-- Host        : gruenau5 running 64-bit openSUSE Leap 15.2
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ proc_tcam_tcam_0_0_stub.vhdl
-- Design      : proc_tcam_tcam_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    clk : in STD_LOGIC;
    rw : in STD_LOGIC;
    write_addr : in STD_LOGIC_VECTOR ( 7 downto 0 );
    write_val : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pattern : in STD_LOGIC_VECTOR ( 7 downto 0 );
    mask : in STD_LOGIC_VECTOR ( 7 downto 0 );
    value : out STD_LOGIC_VECTOR ( 7 downto 0 );
    address : out STD_LOGIC_VECTOR ( 7 downto 0 );
    match_found : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,rw,write_addr[7:0],write_val[7:0],pattern[7:0],mask[7:0],value[7:0],address[7:0],match_found";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "tcam,Vivado 2018.2";
begin
end;
