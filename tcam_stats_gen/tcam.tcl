# Synopsys TCL Script zur Synthese mit CLASS Bibliotheken
#########################################################
set echo_include_commands     "false"
set view_script_submenu_items [list "remove all designs" "remove_design -designs"]
define_design_lib work -path WORK;

# CLASS:
set search_path    [list . /usr/local/echip/Synopsys-2015/libraries/syn]

set target_library {class.db}
set symbol_library {class.sdb generic.sdb}
set link_library   [concat "*" $target_library ]
suppress_message  CMD-041

# define DESIGN source file
puts $variant
set DESIGN $variant

set edifout_design_name   $DESIGN
set designer              {author}
set company               {HUB SKE}
set part                  $DESIGN

# Analyse VHDL (or Verilog) Code
analyze -format vhdl [format "%s.vhd" $DESIGN]

# generate RTL Design
elaborate $DESIGN
#elaborate $DESIGN -parameters "N = 16"

# define TOP with N parameter
#set TOP count2_N16
set TOP $variant

current_design $TOP

# generate unique component names
uniquify

# set_wire_load_model -name 5k_wl

#set_max_leakage_power  1 mW
#set_max_area 80000

#set_max_fanout 150 $TOP
#set_max_capacitance 15.0 $TOP
#set_max_capacitance 35 [get_ports {clk_ecl}]

# Create Clock constraints
#create_clock CLK -period 20
#set_clock_latency 10.2 -rise [get_clocks clk_ecl]
#set_clock_latency 10.2 -fall [get_clocks clk_ecl]
#set_clock_uncertainty 10.0 [get_clocks clk_ecl]
#set_propagated_clock [get_clocks clk_ecl]

#set_dont_touch_network "CLK"
#set_ideal_network {CLK} -no_propagate

#set_dont_touch_network "rstn"
#set_ideal_network {rstn} -no_propagate


#set_structure true -boolean true -boolean_effort high -timing true
compile -incremental_mapping -map_effort high -ungroup_all -power_effort high 


check_design

set_fix_multiple_port_nets -all
set verilogout_no_tri true

# Generate output files
#
# write -xg_force_db -format db -hierarchy -output [format "%s.db" $TOP]
write -format ddc -hierarchy -output [format "%s_1.ddc" $TOP]

write -hierarchy -format vhdl -output tcam.vhd
write -hierarchy -format verilog -output tmp.v
echo "`timescale 1 ns / 1 ps" > [format "%s.v" $TOP]
sh cat tmp.v >> [format "%s.v" $TOP]
sh rm tmp.v
write_sdf [format "%s.sdf" $TOP]
write_sdc [format "%s.sdc" $TOP]


# Write the design report files.                    #
sh rm -rf reports_class
sh mkdir reports_class
  report_area > [format "%s.report.area" $TOP]
  report_timing >  [format "%s.report.timing" $TOP]
  report_timing -path full_clock >  [format "%s.report.timing.clk" $TOP]
  report_clock_timing -clock clk_ecl -type summary -verbose > [format "%s.report.clock" $TOP]	 
  report_power > [format "%s.report.power" $TOP]
  report_constraint -verbose > [format "%s.report.constraint" $TOP]
  report_reference > [format "%s.report.reference" $TOP]
  report_cell > [format "%s.report.cells" $TOP]
  report_port > [format "%s.report.port" $TOP]
  report_net > [format "%s.report.net" $TOP]
  report_net -verbose -connections > [format "%s.report.net.verbose" $TOP]
  report_hierarchy > [format "%s.report.hierarchy" $TOP]
  report_wire_load > [format "%s.report.wire_load" $TOP]
  write_script > [format "%s.report.dc" $TOP]
  report_constraint -all_violators > [format "%s.report.constraints.violators" $TOP]

sh mv *.report.* reports_class

sh rm -rf designs_class
sh mkdir designs_class
sh mv *.ddc designs_class

sh rm -rf netlists_class
sh mkdir netlists_class
sh mv *.v netlists_class

sh rm -rf sdf_class
sh mkdir sdf_class
sh mv *.sdf* *.sdc*  sdf_class

sh rm -f default.svf

exit
