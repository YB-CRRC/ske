#!/usr/bin/env bash
V=2015
M=10.1b
B=`getconf LONG_BIT`
#ECHIP=/usr/local/echip
ECHIP=/vol/repl331-vol2/echip
SYNOPSYS=${ECHIP}/Synopsys-${V}
# ECHIP=/vol/echip
# path = ( $path $SYNOPSYS/bin)
export SNPSLMD_LICENSE_FILE=2100@ti-license
export SYNOPSYS_KEY_FILE=2100@ti-license
export PATH=$SYNOPSYS/bin:$PATH
export LM_LICENSE_FILE="2100@ti-license"
echo using license: $LM_LICENSE_FILE
# Modelsim Environment
export MODEL_TECH=${ECHIP}/Modelsim_${M}/modeltech/bin
export PATH=$PATH:${MODEL_TECH}
# export MODELSIM=${ECHIP}/Modelsim_${M}/modeltech/linux_${B}/modelsim.ini
export MTI_VCO_MODE=${B}

## !/bin/csh
## alternative Einstellen der Umgebungsparameter
##
## source /usr/local/echip/etc/settings.synopsys
## source /usr/local/echip/etc/settings.modelsim101

# */
rm -R scsim.db.dir/*
#*/
rm -R WORK/*
#*/

# Synopsys design compiler
# dc_shell -tcl_mode -f count_gates.tcl
dc_shell -f tcam.tcl -x "set variant $1"

# Modelsim Simulator mit eigener script Datei *.do
# vsim -do modelsim_gates.do
#  Aufruf erfolgt separat in run-modelsim-class.sh 
