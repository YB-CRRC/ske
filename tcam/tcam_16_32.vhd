library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity tcam_16_32 is
  port (
    -- INPUT
    clk: in STD_LOGIC; -- clock
    rw: in STD_LOGIC; -- '0' => read mode , '1' write mode
    write_addr: in STD_LOGIC_VECTOR(15 downto 0); -- address for write mode
    write_val: in STD_LOGIC_VECTOR(31 downto 0); -- value for write mode
    pattern: in STD_LOGIC_VECTOR(31 downto 0); -- pattern for read mode
    mask: in STD_LOGIC_VECTOR(31 downto 0); -- mask for read mode
    
    -- OUTPUT
    value: out STD_LOGIC_VECTOR(31 downto 0); -- read value
    address: out STD_LOGIC_VECTOR(15 downto 0); -- address of read value
    match_found: out STD_LOGIC -- indicates whether a match has been found in read mode
  );
end tcam_16_32;

architecture tcam_16_32_arch of tcam_16_32 is 

  component tcam is
    generic (
      ADDR_WIDTH: INTEGER := 8;
      DATA_WIDTH: INTEGER := 8
    );
    port (
      -- INPUT
      clk: in STD_LOGIC; -- clock
      rw: in STD_LOGIC; -- '0' => read mode , '1' write mode
      write_addr: in STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0); -- address for write mode
      write_val: in STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- value for write mode
      pattern: in STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- pattern for read mode
      mask: in STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- mask for read mode
    
      -- OUTPUT
      value: out STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- read value
      address: out STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0); -- address of read value
      match_found: out STD_LOGIC -- indicates whether a match has been found in read mode
    );
  end component tcam;
  
  constant ADDR_WIDTH: INTEGER := 16;
  constant DATA_WIDTH: INTEGER := 32;

begin
  
  tcam_glue: tcam 
  generic map(ADDR_WIDTH, DATA_WIDTH)
  port map (
    clk => clk,
    rw => rw,
    write_addr => write_addr,
    write_val => write_val,
    pattern => pattern,
    value => value,
    mask => mask,
    address => address,
    match_found => match_found
  );

end tcam_16_32_arch;
