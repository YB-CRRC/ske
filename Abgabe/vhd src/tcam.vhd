library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity tcam is
  generic (
    ADDR_WIDTH: INTEGER := 8;
    DATA_WIDTH: INTEGER := 8
  );
  port (
    -- INPUT
    clk: in STD_LOGIC; -- clock
    rw: in STD_LOGIC; -- '0' => read mode , '1' write mode
    write_addr: in STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0); -- address for write mode
    write_val: in STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- value for write mode
    pattern: in STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- pattern for read mode
    mask: in STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- mask for read mode
    
    -- OUTPUT
    value: out STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- read value
    address: out STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0); -- address of read value
    match_found: out STD_LOGIC -- indicates whether a match has been found in read mode
  );
end tcam;

architecture arch_tcam of tcam is
  constant MAX_ADDR : INTEGER := 2**ADDR_WIDTH - 1;
  type MEMARRAY is array (0 to MAX_ADDR) of std_logic_vector(DATA_WIDTH - 1 downto 0);
  
  signal memory: MEMARRAY := (Others => (Others => '0'));
  signal match_found_internal: STD_LOGIC := '0';
  signal value_internal: STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0) := (Others => '0');
  signal address_internal: STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0) := (Others => '0');
    
begin
  -- READ MODE
  process (pattern, mask, rw, match_found_internal) begin
    address_internal <= (Others => '0');
    value_internal <= (Others => '0');
    match_found_internal <= '0';
    
    -- go through all possible addresses
    for addr in 0 to MAX_ADDR loop
      if (memory(addr) and mask) = (pattern and mask) then
        match_found_internal <= '1';
        value_internal <= memory(addr);
        address_internal <= std_logic_vector(to_unsigned(addr, address'length));
      end if;
    end loop;
  end process;
  
  -- ON CLOCK TICK
  process (clk) begin
    if rising_edge(clk) then
      if rw = '0' then 
        -- if in read mode, publish internal values
        value <= value_internal;
        address <= address_internal;
        match_found <= match_found_internal;
      else 
        memory(to_integer(unsigned(write_addr))) <= write_val;
      end if;
    end if;
  end process;
  
end architecture;
