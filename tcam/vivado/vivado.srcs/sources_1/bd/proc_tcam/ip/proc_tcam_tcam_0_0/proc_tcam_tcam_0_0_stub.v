// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Tue Feb 16 14:39:43 2021
// Host        : gruenau5 running 64-bit openSUSE Leap 15.2
// Command     : write_verilog -force -mode synth_stub
//               /vol/fob-vol7/mi19/bornhoea/ske/tcam/vivado/vivado.srcs/sources_1/bd/proc_tcam/ip/proc_tcam_tcam_0_0/proc_tcam_tcam_0_0_stub.v
// Design      : proc_tcam_tcam_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "tcam,Vivado 2018.2" *)
module proc_tcam_tcam_0_0(clk, rw, write_addr, write_val, pattern, mask, value, 
  address, match_found)
/* synthesis syn_black_box black_box_pad_pin="clk,rw,write_addr[7:0],write_val[7:0],pattern[7:0],mask[7:0],value[7:0],address[7:0],match_found" */;
  input clk;
  input rw;
  input [7:0]write_addr;
  input [7:0]write_val;
  input [7:0]pattern;
  input [7:0]mask;
  output [7:0]value;
  output [7:0]address;
  output match_found;
endmodule
