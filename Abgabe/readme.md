# Schaltkreisentwurf: Projekt TCAM

## Autoren
* Yannic Behrendt <behrendy@hu-berlin.de>
* Anton Bornhöft <bornhoea@hu-berlin.de>

## Projektbeschreibung
Wir haben eine TCAM Implementation geschrieben, bei der die sowohl die
Addressbreite als auch die Datenbreite generisch sind und separat angepasst
werden. Außerdem haben wir Wrapper für verschiedene Kombinationen der
generischen Parameter geschrieben, die für die Synthese in Synopsys bzw. Vivado
verwendet werden können. Anschließend haben wir von Synopsys Reports zu den 
verschiedenen Varianten generieren lassen und eine Variante in ein vollständiges
Prozessordesign in Vivado eingefügt.

## VDHL Code
Im Ordner `vhd src` können folgende Dateien gefunden werden:

*  `tcam.vhd` - Die generische Implementation des TCAMs
*  `tcam_testbench.vhd` - Die Testbench
*  `tcam_Addressbreite_Datenbreite.vhd` - Wrapper für verschiedene Address- & Datenbreiten

## Messungen mit Synopsys
Die Datei `TCAM Messdaten.pdf` enthält Messdaten und Diagramme der verschiedenen
TCAM Varianten. Gemessen wurden dabei die 10 Kombinationen mit Addressbreiten 
von 4 und 8 sowie Datenbreiten von 4, 8, 16, 32 und 64. Für die Varianten mit
Addressbreiten von 16 und höher konnte Synopsys kein Design kompilieren, da die
Komplexität zu hoch wäre, daher wurden für diese Varianten keine Daten erfasst.

## Prozessordesign & C-Testcode
Die Variante `tcam_8_8` wurde in ein vollständiges Prozessordesign eingebettet
und mit C-Code erfolgreich getestet. Der dafür verwendete Code kann in der Datei
`tcam_8_8_testcode.c` gefunden werden.

## Schematics
Die beiden Bilder `tcam_4_4_schematic.png` und `tcam_8_8_schematic.png` zeigen
jeweils den von Synopsys generierten Schaltplan für die Implemenationen 
`tcam_4_4` und `tcam_8_8`. Im erstgenannten Schaltplan sind die Bestandteile des 
TCAMs gut zu erkennen. 16 4-Bit Register können z.B. erkannt werden. Der 
Schaltplan für `tcam_8_8` ist sehr komplex und daher unübersichtlich, 
veranschaulicht aber gut, dass die höheren Address- und Datenbreiten zu deutlich
mehr Komplexität führen.

## Git Repository mit den Projektdaten
Das vollstänige Git Repository kann hier gefunden werden:
[Projektrepository](https://www.bitbucket.org/YB-CRRC/ske)

Das Repository enthält unter Anderem:

- Alle VHDL Source Code Dateien
- Alle von Synopsys generierten Zwischendateien und Reports
- Alle für das Prozessordesign von Vivado generierten Dateien