
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tcam_testbench is
end tcam_testbench;

architecture arch_tcam_testbench of tcam_testbench is
  component tcam is
    generic (
      ADDR_WIDTH: INTEGER := 8;
      DATA_WIDTH: INTEGER := 8
    );
    port (
      -- INPUT
      clk: in STD_LOGIC; -- clock
      rw: in STD_LOGIC; -- '0' => read mode , '1' write mode
      write_addr: in STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0); -- address for write mode
      write_val: in STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- value for write mode
      pattern: in STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- pattern for read mode
      mask: in STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- mask for read mode
      
      -- OUTPUT
      value: out STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- read value
      address: out STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0); -- address of read value
      match_found: out STD_LOGIC -- indicates whether a match has been found in read mode
    );
  end component tcam;
  
  -- CONSTANTS
  constant CLK_PERIOD: TIME := 10ns;
  constant ADDR_WIDTH: INTEGER := 32;
  constant DATA_WIDTH: INTEGER := 32;
    
  -- SIGNALS
  signal clk: STD_LOGIC;
  signal rw: STD_LOGIC;
  signal write_addr: STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0);
  signal write_val: STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
  signal pattern: STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
  signal mask: STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
  signal value: STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
  signal address: STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0);
  signal match_found: STD_LOGIC;
  
  begin
    
    test: process
    variable ref_val: STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
    variable ref_addr: STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0);
    begin
      -- initialize everything to 0
      clk <= '0';
      rw <= '0';
      write_addr <= (Others => '0');
      write_val <= (Others => '0');
      mask <= (Others => '1');
      pattern <= (1 => '1', Others => '0');
      wait for 10ns;
      pattern <= (Others => '0');
      wait for 10ns;
      
      -- try to read a zero
      clk <= '1';
      wait for 10ns;
      clk <= '0';
      wait for 10ns;
      
      ref_val := (Others => '0');
      ref_addr := (Others => '0');
      assert match_found = '1' report "could not find value '0' at start" severity error;
      -- assert address = ref_addr report "could not find value '0' at start" severity error;
      assert value = ref_val report "could not find value '0' at start" severity error;
      
      -- write a value
      wait for 10ns;
      rw <= '1';
      write_addr <= std_logic_vector(to_unsigned(17, ADDR_WIDTH));
      write_val <= std_logic_vector(to_unsigned(42, DATA_WIDTH));
      wait for 10ns;
      
      clk <= '1';
      wait for 10ns;
      clk <= '0';
      wait for 10ns;
      
      -- read the written value
      rw <= '0';
      pattern <= std_logic_vector(to_unsigned(42, DATA_WIDTH));
      
      wait for 10ns;
      clk <= '1';
      wait for 10ns;
      clk <= '0';
      wait for 10ns;
      
      ref_val := std_logic_vector(to_unsigned(42, DATA_WIDTH));
      ref_addr := std_logic_vector(to_unsigned(17, ADDR_WIDTH));
      assert match_found = '1' report "could not find value" severity error;
      assert address = ref_addr report "could not find value" severity error;
      assert value = ref_val report "could not find value" severity error;
      
      wait;   
    end process;
    
    DUT: tcam 
    generic map (ADDR_WIDTH, DATA_WIDTH)
    port map (
      clk => clk,
      rw => rw,
      write_addr => write_addr,
      write_val => write_val,
      pattern => pattern,
      value => value,
      mask => mask,
      address => address,
      match_found => match_found
    );
    
end arch_tcam_testbench;

configuration tcam_test of tcam_testbench is
  for arch_tcam_testbench
    for DUT:tcam
      use entity work.tcam;
    end for;
  end for;
end configuration;
