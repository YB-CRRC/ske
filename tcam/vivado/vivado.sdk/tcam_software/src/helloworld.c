/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "sleep.h"
#include "xil_printf.h"

#define WRITE_MODE 1;
#define READ_MODE 0;

unsigned char* TCAM_CLK         = (unsigned char*) XPAR_AXI_GPIO_CLK_BASEADDR;
unsigned char* TCAM_RW          = (unsigned char*) XPAR_AXI_GPIO_RW_BASEADDR;
unsigned char* TCAM_WRITE_ADDR  = (unsigned char*) XPAR_AXI_GPIO_WRITE_ADDR_BASEADDR;
unsigned char* TCAM_WRITE_VAL   = (unsigned char*) XPAR_AXI_GPIO_WRITE_VAL_BASEADDR;
unsigned char* TCAM_PATTERN     = (unsigned char*) XPAR_AXI_GPIO_PATTERN_BASEADDR;
unsigned char* TCAM_MASK        = (unsigned char*) XPAR_AXI_GPIO_MASK_BASEADDR;
unsigned char* TCAM_ADDRESS     = (unsigned char*) XPAR_AXI_GPIO_ADDRESS_BASEADDR;
unsigned char* TCAM_VALUE       = (unsigned char*) XPAR_AXI_GPIO_VALUE_BASEADDR;
unsigned char* TCAM_MATCH_FOUND = (unsigned char*) XPAR_AXI_GPIO_MATCH_FOUND_BASEADDR;

void write(unsigned char addr, unsigned char value);
int read(unsigned char pattern, unsigned char mask, unsigned char* addr, unsigned char* value);
void synchronize();

int main()
{
    init_platform();

    unsigned char read_val = 0;
    unsigned char read_addr = 0;

    int match_found = read(0, 0xFF, &read_addr, &read_val);
    xil_printf("read: %d %d %d\r\n", match_found, read_addr, read_val);

    write(17, 42);
    write(96, 42);

    match_found = read(42, 0xFF, &read_addr, &read_val);
    xil_printf("read: %d %d %d\r\n", match_found, read_addr, read_val);

    match_found = read(120, 0xFF, &read_addr, &read_val);
    xil_printf("read: %d %d %d\r\n", match_found, read_addr, read_val);

    cleanup_platform();
    return 0;
}

void write(unsigned char addr, unsigned char value) {
	*TCAM_WRITE_ADDR = addr;
	*TCAM_WRITE_VAL = value;
	*TCAM_RW = WRITE_MODE;
	synchronize();
}

int read(unsigned char pattern, unsigned char mask, unsigned char* addr, unsigned char* value) {
	*TCAM_RW = READ_MODE;
	*TCAM_PATTERN = pattern;
	*TCAM_MASK = mask;
	synchronize();

	unsigned char match_found = *TCAM_MATCH_FOUND;

	if (match_found) {
		*addr = *TCAM_ADDRESS;
		*value = *TCAM_VALUE;
	}

	return (int) match_found;
}

void synchronize() {
	*TCAM_CLK = 0;
	*TCAM_CLK = 1;
	*TCAM_CLK = 0;
}
