#If required use the below command and launch symbol server from an external shell.
#symbol_server -S -s tcp::1534
connect -path [list tcp::1534 tcp:dslab04:3121]
source /vol/fob-vol7/mi19/bornhoea/ske/tcam/vivado/vivado.sdk/proc_tcam_wrapper_hw_platform_0/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Xilinx PYNQ-Z1 003017ADA590A"} -index 0
loadhw -hw /vol/fob-vol7/mi19/bornhoea/ske/tcam/vivado/vivado.sdk/proc_tcam_wrapper_hw_platform_0/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Xilinx PYNQ-Z1 003017ADA590A"} -index 0
stop
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Xilinx PYNQ-Z1 003017ADA590A"} -index 0
rst -processor
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Xilinx PYNQ-Z1 003017ADA590A"} -index 0
dow /vol/fob-vol7/mi19/bornhoea/ske/tcam/vivado/vivado.sdk/tcam_software/Debug/tcam_software.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Xilinx PYNQ-Z1 003017ADA590A"} -index 0
con
